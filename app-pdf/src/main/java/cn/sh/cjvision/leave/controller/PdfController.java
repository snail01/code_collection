package cn.sh.cjvision.leave.controller;

import cn.sh.cjvision.framework.utils.out.OutPutJson;
import cn.sh.cjvision.leave.service.LxBlqkService;
import cn.sh.cjvision.leave.utils.pdf.ConvertMoney;
import cn.sh.cjvision.leave.utils.pdf.ConvertUtil;
import cn.sh.cjvision.leave.utils.pdf.PdfUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: zhangzhanhang
 * Email zhangzhanhang@jingyuonline.com
 * Date: 20-02-18
 * Time: 16:01
 * Description:
 */
@Api(tags="pdf打印")
@RestController
@RequestMapping("xggl/print")
public class PdfController {

    private final static Logger log = LoggerFactory.getLogger(PdfController.class);

    @Autowired
    private LxBlqkService lxBlqkService;

    @ApiOperation(value = "财务缴费pdf")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pcid", value = "批次id", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "xh", value = "学号", paramType = "query", dataType = "string")})
    @GetMapping("/cwjf")
    public void cwjf(String xh, String pcid, HttpServletRequest request,HttpServletResponse response){
        InputStream inputStream = null;
        ServletOutputStream servletOutputStream = null;

        try {
            StringBuffer sql=new StringBuffer("select distinct pc.MC PCMC,blqk.PCID,xs.XH,xs.XM,xb.NAME XBMC,sf.YYF,sf.XF,'' ZJE,blqk.LXBL,blqk.CWJF,team.TEAMNAME ");
            sql.append(" from LX_BLQK blqk inner join LX_LXPC pc on pc.ID=blqk.PCID and pc.STATUSID='1' inner join T_XS_JBXX xs on xs.XH=blqk.XSID ");
            sql.append(" left join LX_SFQK sf on sf.XH=xs.XH and sf.PCID=blqk.PCID left join STD_GB_XB xb on xb.CODE=xs.XBDM ");
            sql.append(" left join T_TRAIN_PLAYER pla on pla.XH=xs.XH and pla.STATE in ('1','4') left join T_TRAIN_RELATION rel on rel.PLAYERID=pla.D_ID_ ");
            sql.append(" and rel.PLAYERSTATE in ('1','4') and rel.TEAMSTATE in ('1','4') ");
            sql.append(" left join T_TRAIN_TEAM team on team.D_ID_=rel.TEAMID and team.STATE in ('1','4') where 1=1 ");
            sql.append(" and blqk.XSID=?0 and blqk.PCID=?1 ");
            List<Map<String,Object>> list=lxBlqkService.findQuery(sql.toString(),new Object[]{xh,pcid});

            //图书费用
            List<Map<String,Object>> tsinfo=lxBlqkService.findQuery("select TSMC SM,JFJE SF from V_TSJHXX WHERE XSID=?0  group by XSID",new Object[]{xh});
            if(list.size()>0){
                Integer yyf=Integer.parseInt(list.get(0).get("YYF")==null?"0":list.get(0).get("YYF").toString());
                Integer xf=Integer.parseInt(list.get(0).get("XF")==null?"0":list.get(0).get("XF").toString());
                Integer ts=0;
                for (int i=0;i<tsinfo.size();i++){
                    ts +=Integer.parseInt(tsinfo.get(i).get("SF")==null?"0":tsinfo.get(i).get("SF").toString());
                }
                list.get(0).put("JE",yyf+xf+ts);
            }

            Map<String,Object> data = new HashMap();
            if(list.size()>0){
                data.put("XH",list.get(0).get("XH")==null?"":list.get(0).get("XH"));
                data.put("XM",list.get(0).get("XM")==null?"":list.get(0).get("XM"));
                data.put("XB",list.get(0).get("XBMC")==null?"":list.get(0).get("XBMC"));
                data.put("YDD",list.get(0).get("TEAMNAME")==null?"":list.get(0).get("TEAMNAME"));
                data.put("XF","￥"+list.get(0).get("XF")==null?"":list.get(0).get("XF"));
                data.put("YYF","￥"+list.get(0).get("YYF")==null?"":list.get(0).get("YYF"));
            }else{
                data.put("XH","");
                data.put("XM","");
                data.put("XB", "");
                data.put("YDD","");
                data.put("XF","");
                data.put("YYF","");
            }
            data.put("JFR","");
            data.put("JFSJ","");
            //总金额
            data.put("JE_CN", data.get("JE")==null?"":ConvertMoney.convert(Double.parseDouble(data.get("JE").toString())));
            data.put("JE",data.get("JE")==null?"":"￥"+data.get("JE").toString());
            //图书信息
            data.put("TSDATA",tsinfo);


            String content = PdfUtils.freeMarkerRender(data,"cwjf_print.html");
            String fileName="财务缴费.pdf";
            // 防止中文乱码
            // 针对IE或者以IE为内核的浏览器：
            String userAgent = request.getHeader("User-Agent");
            if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
                fileName = java.net.URLEncoder.encode(fileName, "UTF-8");
            } else {
                // 非IE浏览器的处理：
                fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
            }
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            response.setContentType("application/octet-stream;charset=UTF-8");
            OutputStream outputStream=new BufferedOutputStream(response.getOutputStream());;
            PdfUtils.createPdf(content,"http://"+request.getServerName(),outputStream);

            outputStream.flush();
        }catch (Exception e){
            log.error("财务缴费打印异常",e);
        }finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.close();
                    servletOutputStream = null;
                }
                if (inputStream != null) {
                    inputStream.close();
                    inputStream = null;
                }
                // 召唤jvm的垃圾回收器
                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @ApiOperation(value = "超期未办理pdf")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pcid", value = "批次id", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "xh", value = "学号", paramType = "query", dataType = "string")})
    @GetMapping("/cqwbl")
    public void cqwbl(String xh, String pcid, HttpServletRequest request,HttpServletResponse response){
        InputStream inputStream = null;
        ServletOutputStream servletOutputStream = null;
        try {
            StringBuffer selectSql=new StringBuffer();
            selectSql.append(" select distinct blqk.ID,pc.MC PCMC,blqk.PCID,blqk.XSID XH,xs.XM XSXM,nj.NAME NJMC,lb.NAME LXLB,team.TEAMNAME,xb.NAME XBMC ");
            selectSql.append(",blqk.LXBL,blqk.BLHS,blqk.GHYKT,blqk.CWJF,blqk.LXDJ,blqk.BLHKQYZM,blqk.HJBL,blqk.SJYS,blqk.LQXJDA,blqk.LQJKKD,blqk.LXSH,blqk.KJSX,pc.KSSJ,xj.ZXZT,xj.XSDQZT ");
            selectSql.append("from LX_BLQK blqk inner join LX_LXPC pc on pc.ID=blqk.PCID ");
            selectSql.append("inner join T_XS_JBXX xs on xs.XH=blqk.XSID left join STD_JB_NJ nj on xs.DQNJ=nj.CODE ");
            selectSql.append("left join STD_LX_LB lb on lb.CODE=blqk.LXLBID left join T_TRAIN_PLAYER pla on pla.XH=blqk.XSID ");
            selectSql.append("left join T_TRAIN_RELATION rel on rel.PLAYERID=pla.XH left join T_TRAIN_TEAM team on team.D_ID_=rel.TEAMID ");
            selectSql.append("left join T_XS_XJXX xj on xj.XH=blqk.XSID left join STD_GB_XB xb on xb.CODE=xs.XBDM ");
            selectSql.append("where 1=1 and blqk.SFCQ='1' and pc.STATUSID='1' ");
            selectSql.append(" and blqk.XSID=?0 and blqk.PCID=?1 ");
            List<Map<String,Object>> mapList=lxBlqkService.findQuery(selectSql.toString(),new Object[]{xh,pcid});
            List<Map<String,Object>> hlhj=lxBlqkService.findQuery("select CODE,NAME from STD_LX_BLQK where STATUS='1' order by SORT");
            mapList.stream().map(x->{
                for (int i=0;i<hlhj.size();i++){
                    if(x.get("LXBL")!=null&&x.get("LXBL").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("LXBL",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("LXSH")!=null&&x.get("LXSH").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("LXSH",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("LXDJ")!=null&&x.get("LXDJ").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("LXDJ",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("BLHS")!=null&&x.get("BLHS").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("BLHS",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("GHYKT")!=null&&x.get("GHYKT").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("GHYKT",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("LQJKKD")!=null&&x.get("LQJKKD").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("LQJKKD",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("HJBL")!=null&&x.get("HJBL").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("HJBL",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("SJYS")!=null&&x.get("SJYS").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("SJYS",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("CWJF")!=null&&x.get("CWJF").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("CWJF",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("BLHKQYZM")!=null&&x.get("BLHKQYZM").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("BLHKQYZM",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("LQXJDA")!=null&&x.get("LQXJDA").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("LQXJDA",hlhj.get(i).get("NAME").toString());
                    }
                    if(x.get("KJSX")!=null&&x.get("KJSX").toString().equals(hlhj.get(i).get("CODE").toString())){
                        x.put("KJSX",hlhj.get(i).get("NAME").toString());
                    }
                }
                return x;
            }).collect(Collectors.toList());
            Map<String,Object> map=new HashMap<>();
            if(mapList.size()>0){
                map.put("XM",mapList.get(0).get("XSXM")==null?"":mapList.get(0).get("XSXM").toString());
                map.put("XB",mapList.get(0).get("XBMC")==null?"":mapList.get(0).get("XBMC").toString());
                map.put("LXRQ","");
                map.put("NJ",mapList.get(0).get("NJMC")==null?"":mapList.get(0).get("NJMC").toString());
                map.put("YDD",mapList.get(0).get("TEAMNAME")==null?"":mapList.get(0).get("TEAMNAME").toString());
                map.put("LXQX","");
                map.put("LXBL",mapList.get(0).get("LXBL")==null?"":mapList.get(0).get("LXBL").toString());
                map.put("LXSH",mapList.get(0).get("LXSH")==null?"":mapList.get(0).get("LXSH").toString());
                map.put("LXDJ",mapList.get(0).get("LXDJ")==null?"":mapList.get(0).get("LXDJ").toString());
                map.put("BLHS",mapList.get(0).get("BLHS")==null?"":mapList.get(0).get("BLHS").toString());
                map.put("GHYKT",mapList.get(0).get("GHYKT")==null?"":mapList.get(0).get("GHYKT").toString());
                map.put("LQJKKD",mapList.get(0).get("LQJKKD")==null?"":mapList.get(0).get("LQJKKD").toString());
                map.put("HJBL",mapList.get(0).get("HJBL")==null?"":mapList.get(0).get("HJBL").toString());
                map.put("SJYS",mapList.get(0).get("SJYS")==null?"":mapList.get(0).get("SJYS").toString());
                map.put("CWJF",mapList.get(0).get("CWJF")==null?"":mapList.get(0).get("CWJF").toString());
                map.put("BLHKQYZM",mapList.get(0).get("BLHKQYZM")==null?"":mapList.get(0).get("BLHKQYZM").toString());
                map.put("LQXJDA",mapList.get(0).get("LQXJDA")==null?"":mapList.get(0).get("LQXJDA").toString());
                map.put("KJSX",mapList.get(0).get("KJSX")==null?"":mapList.get(0).get("KJSX").toString());
            }else{
                map.put("XM","");
                map.put("XB","");
                map.put("LXRQ","");
                map.put("NJ","");
                map.put("YDD","");
                map.put("LXQX","");
                map.put("LXBL","");
                map.put("LXSH","");
                map.put("LXDJ","");
                map.put("BLHS","");
                map.put("GHYKT","");
                map.put("LQJKKD","");
                map.put("HJBL","");
                map.put("SJYS","");
                map.put("CWJF","");
                map.put("BLHKQYZM","");
                map.put("LQXJDA","");
                map.put("KJSX","");
            }

            String content = PdfUtils.freeMarkerRender(map,"lxsh_print.html");
            String fileName="超期未办理.pdf";
            // 防止中文乱码
            // 针对IE或者以IE为内核的浏览器：
            String userAgent = request.getHeader("User-Agent");
            if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
                fileName = java.net.URLEncoder.encode(fileName, "UTF-8");
            } else {
                // 非IE浏览器的处理：
                fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
            }
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            response.setContentType("application/octet-stream;charset=UTF-8");
            OutputStream outputStream=new BufferedOutputStream(response.getOutputStream());;
            PdfUtils.createPdf(content,"http://"+request.getServerName(),outputStream);

            outputStream.flush();
        }catch (Exception e){
            log.error("超期未办理打印异常",e);
        }finally {
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.close();
                    servletOutputStream = null;
                }
                if (inputStream != null) {
                    inputStream.close();
                    inputStream = null;
                }
                // 召唤jvm的垃圾回收器
                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
