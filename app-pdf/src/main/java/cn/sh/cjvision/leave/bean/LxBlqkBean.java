package cn.sh.cjvision.leave.bean;

import cn.sh.cjvision.framework.utils.constant.AppConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


/**
 * 离校系统环节办理情况
 *
 * @author zf
 * @email zhangfan@jingyuonline.com
 * @date 2020-02-10 15:14:31
 */
@ApiModel(value = "离校系统环节办理情况")
@Entity
@Table(name = "LX_BLQK")
public class LxBlqkBean {

    /**
     * 办理还书
     */
    @ApiModelProperty(value = "ID值")
    @Column(name = "BLHS", length = 64)
    private String blhs;

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(generator = "D_ID_")
    @GenericGenerator(name = "D_ID_", strategy = AppConstants.SEQ_GENERATOR_STR)
    @ApiModelProperty(value = "主键ID")
    @Column(name = "ID", length = 64)
    private String id;

    /**
     * 归还一卡通
     */
    @ApiModelProperty(value = "归还一卡通")
    @Column(name = "GHYKT", length = 64)
    private String ghykt;

    /**
     * 财务交费
     */
    @ApiModelProperty(value = "财务交费")
    @Column(name = "CWJF", length = 64)
    private String cwjf;

    /**
     * 离校登记
     */
    @ApiModelProperty(value = "离校登记")
    @Column(name = "LXDJ", length = 64)
    private String lxdj;

    /**
     * 办理户口迁移证明
     */
    @ApiModelProperty(value = "办理户口迁移证明")
    @Column(name = "BLHKQYZM", length = 64)
    private String blhkqyzm;

    /**
     * 户籍办理
     */
    @ApiModelProperty(value = "户籍办理")
    @Column(name = "HJBL", length = 64)
    private String hjbl;

    /**
     * 上交钥匙
     */
    @ApiModelProperty(value = "上交钥匙")
    @Column(name = "SJYS", length = 64)
    private String sjys;

    /**
     * 领取学籍档案
     */
    @ApiModelProperty(value = "领取学籍档案")
    @Column(name = "LQXJDA", length = 64)
    private String lqxjda;

    /**
     * 领取健康卡等
     */
    @ApiModelProperty(value = "领取健康卡等")
    @Column(name = "LQJKKD", length = 64)
    private String lqjkkd;

    /**
     * 离校审核
     */
    @ApiModelProperty(value = "离校审核")
    @Column(name = "LXSH", length = 64)
    private String lxsh;

    /**
     * 开介绍信
     */
    @ApiModelProperty(value = "开介绍信")
    @Column(name = "KJSX", length = 64)
    private String kjsx;

    /**
     * 离校办理
     */
    @ApiModelProperty(value = "离校办理")
    @Column(name = "LXBL", length = 64)
    private String lxbl;

    /**
     * 学生ID
     */
    @ApiModelProperty(value = "学生ID")
    @Column(name = "XSID", length = 64)
    private String xsid;

    /**
     * 批次ID
     */
    @ApiModelProperty(value = "批次ID")
    @Column(name = "PCID", length = 64)
    private String pcid;

    /**
     * 离校类别ID
     */
    @ApiModelProperty(value = "离校类别ID")
    @Column(name = "LXLBID", length = 64)
    private String lxlbid;


    /**
     * 附件id
     */
    @ApiModelProperty(value = " 附件id")
    @Transient
    private String fjid;


    /**
     * 办理还书
     *
     * @return
     */
    public String getBlhs() {
        return blhs;
    }

    /**
     * 办理还书
     *
     * @param blhs
     */
    public void setBlhs(String blhs) {
        this.blhs = blhs;
    }

    /**
     * 归还一卡通
     */
    public String getGhykt() {
        return ghykt;

    }

    /**
     * 归还一卡通
     */
    public void setGhykt(String ghykt) {
        this.ghykt = ghykt;

    }


    /**
     * 财务交费
     */
    public String getCwjf() {
        return cwjf;

    }

    /**
     * 财务交费
     */
    public void setCwjf(String cwjf) {
        this.cwjf = cwjf;

    }


    /**
     * 离校登记
     */
    public String getLxdj() {
        return lxdj;

    }

    /**
     * 离校登记
     */
    public void setLxdj(String lxdj) {
        this.lxdj = lxdj;

    }


    /**
     * 办理户口迁移证明
     */
    public String getBlhkqyzm() {
        return blhkqyzm;

    }

    /**
     * 办理户口迁移证明
     */
    public void setBlhkqyzm(String blhkqyzm) {
        this.blhkqyzm = blhkqyzm;

    }


    /**
     * 户籍办理
     */
    public String getHjbl() {
        return hjbl;

    }

    /**
     * 户籍办理
     */
    public void setHjbl(String hjbl) {
        this.hjbl = hjbl;

    }


    /**
     * 上交钥匙
     */
    public String getSjys() {
        return sjys;

    }

    /**
     * 上交钥匙
     */
    public void setSjys(String sjys) {
        this.sjys = sjys;

    }


    /**
     * 主键ID
     */
    public String getId() {
        return id;

    }

    /**
     * 主键ID
     */
    public void setId(String id) {
        this.id = id;

    }


    /**
     * 领取学籍档案
     */
    public String getLqxjda() {
        return lqxjda;

    }

    /**
     * 领取学籍档案
     */
    public void setLqxjda(String lqxjda) {
        this.lqxjda = lqxjda;

    }


    /**
     * 领取健康卡等
     */
    public String getLqjkkd() {
        return lqjkkd;

    }

    /**
     * 领取健康卡等
     */
    public void setLqjkkd(String lqjkkd) {
        this.lqjkkd = lqjkkd;

    }


    /**
     * 离校审核
     */
    public String getLxsh() {
        return lxsh;

    }

    /**
     * 离校审核
     */
    public void setLxsh(String lxsh) {
        this.lxsh = lxsh;

    }


    /**
     * 开介绍信
     */
    public String getKjsx() {
        return kjsx;

    }

    /**
     * 开介绍信
     */
    public void setKjsx(String kjsx) {
        this.kjsx = kjsx;

    }


    /**
     * 离校办理
     */
    public String getLxbl() {
        return lxbl;

    }

    /**
     * 离校办理
     */
    public void setLxbl(String lxbl) {
        this.lxbl = lxbl;

    }


    /**
     * 学生ID
     */
    public String getXsid() {
        return xsid;

    }

    /**
     * 学生ID
     */
    public void setXsid(String xsid) {
        this.xsid = xsid;

    }


    /**
     * 批次ID
     */
    public String getPcid() {
        return pcid;

    }

    /**
     * 批次ID
     */
    public void setPcid(String pcid) {
        this.pcid = pcid;

    }


    /**
     * 离校类别ID
     */
    public String getLxlbid() {
        return lxlbid;

    }

    /**
     * 离校类别ID
     */
    public void setLxlbid(String lxlbid) {
        this.lxlbid = lxlbid;

    }


    public String getFjid() {
        return fjid;
    }

    public void setFjid(String fjid) {
        this.fjid = fjid;
    }


}
