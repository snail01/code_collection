package cn.sh.cjvision.leave.utils.pdf;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * @author: zhangzhanhang
 * Email zhangzhanhang@jingyuonline.com
 * Date: 20-02-18
 * Time: 16:06
 * Description:
 */
public class PdfUtils {

    private static Configuration freemarkerCfg = null;

    static  {
        freemarkerCfg =new Configuration();
        //freemarker的模板目录
        try {
            freemarkerCfg.setDefaultEncoding("utf-8");
//            freemarkerCfg.setDirectoryForTemplateLoading(new File(PathUtil.getCurrentPath()));
            freemarkerCfg.setClassForTemplateLoading(PdfUtils.class,"/template");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws IOException, DocumentException {

        cwjf();
    }
    private static void cwjf() throws IOException, DocumentException {
        Map<String,Object> data = new HashMap();
        data.put("XH","123456789");
        data.put("XM","测试姓名");
        data.put("XB", "男");
        data.put("YDD","运动队1");

        data.put("XF","￥10");
        data.put("YYF","￥10");
        data.put("JE","￥10");
        data.put("JE_CN","￥10");
        data.put("JFR","交费人");
        data.put("JFSJ","2020-02-10");

        JSONArray array=new JSONArray();
        JSONObject object=new JSONObject();
        object.put("SM","1");object.put("SF","2");
        JSONObject object1=new JSONObject();
        object1.put("SM","1");object1.put("SF","2");
        JSONObject object2=new JSONObject();
        object2.put("SM","1");object2.put("SF","2");
        JSONObject object3=new JSONObject();
        object3.put("SM","1");object3.put("SF","2");
        JSONObject object4=new JSONObject();
        object4.put("SM","1");object4.put("SF","2");
        JSONObject object5=new JSONObject();
        object5.put("SM","1");object5.put("SF","2");
        JSONObject object6=new JSONObject();
        object6.put("SM","1");object6.put("SF","2");
        array.add(object);array.add(object1);array.add(object2);array.add(object3);array.add(object4);array.add(object5);array.add(object6);
        data.put("TSDATA",array);
        String content = freeMarkerRender(data,"/cwjf_print.html");
        OutputStream outputStream=null;
        createPdf(content,"",outputStream);


    }


    public static void createPdf(String content,String footer,OutputStream outputStream) throws IOException, DocumentException {
        // step 1
        Document document = new Document(PageSize.A4, 30, 30, 30, 30);
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);

        PDFBuilder headerFooter=new PDFBuilder();
        headerFooter.setFooter(footer);
        writer.setPageEvent(headerFooter);
        // step 3
        document.open();
        // step 4
        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                new ByteArrayInputStream(content.getBytes("UTF-8")), null, Charset.forName("UTF-8"), new FontProviderUtil());
        // step 5
        document.close();
    }

    /**
     * freemarker渲染html
     */
    public static String freeMarkerRender(Map<String, Object> data, String htmlTmp) {
        Writer out = new StringWriter();
        try {
            // 获取模板,并设置编码方式
            Template template = freemarkerCfg.getTemplate(htmlTmp);
            template.setEncoding("UTF-8");
            // 合并数据模型与模板
            //将合并后的数据和模板写入到流中，这里使用的字符流
            template.process(data, out);
            out.flush();
            return out.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}
