package cn.sh.cjvision.leave.service;

import cn.sh.cjvision.framework.repository.CustomRepository;
import cn.sh.cjvision.leave.bean.LxBlqkBean;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor = Exception.class)
public interface LxBlqkService extends CustomRepository<LxBlqkBean, String> {

    List<LxBlqkBean> findAllByPcid(String pcid);

    void deleteAllByPcidAndXsidIn(String pcid, List<String> xhs);

    @Modifying
    @Query(value = "update LX_BLQK set LXBL = ?1 where ID = ?2 ",nativeQuery = true)
    void updateLxbl(String status, String id);

    @Modifying
    @Query(value = "update LX_BLQK set LXSH = ?1 where ID = ?2 ",nativeQuery = true)
    void updateLxsh(String status, String id);

    @Modifying
    @Query(value = "update LX_BLQK set ?1 = '1' where ID in ?2 ",nativeQuery = true)
    void updateprocess(String status, List<String> id);
}